let instructions = {
  '0' : {
    'message' : "Copie le mot mémoire d’adresse {adresse} dans le registre A",
    'operandes' : "adresse",
    'fonction' : function(adresse){
      if (adresse == "99"){
        let valeur = prompt("Entrez une valeur");
        set(99, valeur);
      }
      logger(this.message.replaceAll("{adresse}", adresse));
      set("A", get(parseInt(adresse)));
      focus_registre("A");
    }
  },
  '1' : {
    'message' : "Copie le mot mémoire d’adresse {adresse} dans le registre B",
    'operandes' : "adresse",
    'fonction' : function(adresse){
        if (adresse == "99"){
          let valeur = prompt("Entrez une valeur");
          set(99, valeur);
        }
        logger(this.message.replaceAll("{adresse}", adresse));
        set("B", get(parseInt(adresse)));
        focus_registre("B");
      }
  },
  '2' : {
    'message' :"Copie le contenu du registre R dans le mot mémoire d'adresse {adresse}",
    'operandes' : "adresse",
    'fonction' : function(adresse){
      logger(this.message.replaceAll("{adresse}", adresse));
      set(parseInt(adresse), get("R"));
      focus_registre("_" + adresse);
      if (adresse == "99"){
        alert(get("R"));
      }
    }
  },
  '3' : {
    'message' :"Effectue une opération arithmétique entre {r1} et {r2}",
    'operandes' : "r1 r2",
    'fonction' : function(operande){
      let rs = registres[operande[0]];
      let rd = registres[operande[1]];
      let a = parseInt(get("A"), 10);
      let b = parseInt(get("B"), 10);

      let resultat = 0;

      if (operande == '99'){
        logger('Aucune action');
        return ;
      } else if (operande == '00'){
          resultat = a + b;
          set("R", resultat.toString().padStart(3, '0'));
          focus_registre("R");
      } else if (operande == '01'){
          resultat = a - b;
          set("R", resultat.toString().padStart(3, '0'));
          focus_registre("R");
      } else if (operande == '02'){
          let quotient = Math.trunc(a / 2);
          let reste = a % 2;
          set("R", quotient.toString().padStart(3, '0'));
          set("B", reste.toString().padStart(3, '0'));
      } else {
          set("R", (a * 2).toString().padStart(3, '0'));
          focus_registre("R");
      }
      logger(this.message.replaceAll("{r1}", rs).replaceAll('{r2}', rd));

    }
  },
  '4' : {
    'message' : "Copie la valeur du registre source {rs} dans le registre destination {rd}",
    'operandes' : "rs rd",
    'fonction' : function(operande){
        let rs = registres[operande[0]];
        let rd = registres[operande[1]];
        logger(this.message.replaceAll("{rs}", rs).replaceAll('{rd}', rd));
        set(rd, get(rs));
        focus_registre(rd);
        }
  },
  '5' : {
    'message' : "Branchement en {adresse} (PC reçoit la valeur {adresse})",
    'operandes' : "adresse",
    'fonction' : function(adresse){
      logger(this.message.replaceAll("{adresse}", adresse));
      set("PC", adresse);
      focus_registre("PC");
      if (adresse == "99"){
        logger("Arrêt de la machine (PC vaut 99)");
      }
    }
  },
  '6' : {
    'message' : "Branchement en {adresse} si la valeur du registre R est strictement positive",
    'operandes' : "adresse",
    'fonction' : function(adresse){
      let R = parseInt(get("R"), 10);
      if (R > 0){
        logger(this.message.replaceAll("{adresse}", adresse));
        set("PC", adresse);
        focus_registre("PC");
        if (adresse == "99"){
          logger("Arrêt de la machine (PC vaut 99)");
        }
      }
    }
  },
  '7' : {
    'message' : "Incrément de la valeur à l'{adresse}",
    'operandes' : "adresse",
    'fonction' : function(adresse){
      let _adresse = parseInt(adresse, 10);
      set(parseInt(adresse), (parseInt(get(_adresse), 10) + 1).toString().padStart(3, '0'));
      logger(this.message.replaceAll("{adresse}", adresse), "red");
      focus_registre("_" + adresse);
    }
  }
};

const _jeu = document.querySelector("#instructions tbody");
for (let operation in instructions) {
  let tr = document.createElement('tr');
  tr.id = "i" + operation;
  tr.innerHTML = "<td>" + operation + "</td><td><i>" + instructions[operation].operandes + "</i></td><td style='text-align:left;'>" + instructions[operation].message + "</td>";
  _jeu.appendChild(tr);
}
