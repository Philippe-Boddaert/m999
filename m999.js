const memoire = document.querySelector("#memoire tbody");
const _console = document.querySelector("#console");

let registres = { '0' : 'A', '1' : 'B', '2' : 'R'};

let tr, th, td, input;

let etat = 0;
let courante = null;
let op = null;

for (let i = 0; i < 10; i++){
  tr = document.createElement('tr');
  th = document.createElement('th');
  th.innerHTML = i;
  tr.appendChild(th);
  for (let j = 0; j < 10; j++){
    td = document.createElement('td');
    input = document.createElement('input');
    input.id = "_" + ((j * 10)+ i);
    input.type = "text";
    input.tabIndex = (j * 10) + i + 1;
    td.appendChild(input);
    tr.appendChild(td);
  }
  th = document.createElement('th');
  th.innerHTML = i;
  tr.appendChild(th);
  memoire.appendChild(tr);
}
td.classList.add("es");

function set(id, value){
  let element = null;
  if (typeof(id) === 'number'){
    element = document.querySelector('#_' + id);
  } else {
    element = document.querySelector('#' + id);
  }
  element.value = value;
}

function get(id){
  let element = null;
  if (typeof(id) === 'number'){
    element = document.querySelector('#_' + id);
  } else {
    element = document.querySelector('#' + id);
  }
  return element.value.padStart(3, '0');
}

function load(data){
  for (let i = 0; i < data.length; i++){
    set(i, data[i]);
  }
}

function raz(){
  for (let i = 0; i < 100; i++){
    set(i, '');
  }
  set("PC", "");
  set("R", "");
  set("A", "");
  set("B", "");
  logger();
  focus_memoire();
  focus_registre();
  focus_instruction();
}

function logger(message = null, couleur="black"){
  if (message == null){
    _console.innerHTML = '';
  } else {

    _console.innerHTML = _console.innerHTML + "<p style='color:" + couleur + ";'>" + message + "<p/>";
    _console.scrollTop = _console.scrollHeight;
  }
}

function incrementer(){
  let courante = parseInt(get("PC"), 10);
  courante++;
  set("PC", courante.toString().padStart(2, '0'));
}

function decoder(mot){
    return [mot[0], mot.substring(1)];
}

function demarrer(){
  raz();
  load(eval(document.querySelector("#datas").value));
  set("PC", "00");
  document.getElementById("_0").focus();
}

function focus(id, type){
  let current = document.querySelector(".focus_" + type);
  if (current != null){
    current.classList.remove('focus_' + type);
  }
  if (id != null){
    document.getElementById(id).classList.add("focus_" + type);
  }
}

function focus_registre(registre = null){
  focus(registre, "registre");
}

function focus_memoire(id = null){
  focus((id == null)?id:("_" + id), "memoire");
}

function focus_instruction(op = null){
  focus((op == null)?op:("i" + op), "instruction");
}

function executer(){
  if(parseInt(get("PC"), 10) != 99){
    switch (etat){
      case 0 :
        courante = get(parseInt(get("PC"), 10));
        focus_memoire(parseInt(get("PC"), 10));
        focus_registre();
        focus_instruction();
        logger("Chargement du mot " + courante + " à l'adresse " + get("PC"), "blue");
        etat = 1;
        break;
      case 1 :
        incrementer();
        focus_registre("PC");
        logger("Incrément de la valeur de PC à " + get("PC"), "red");
        etat = 2;
        break;
      case 2 :
        op = decoder(courante);
        focus_instruction(op[0]);
        logger("Décodage de l'instruction (opération : " + op[0] + ", operandes :"+ op[1] + ")", "green");
        etat = 3;
        break;
      case 3 :
        instructions[op[0]].fonction(op[1]);
        etat = 0;
        break;
    }
  }
}

document.querySelector("#demarrer").addEventListener('click', demarrer);
document.querySelector("#lancer").addEventListener('click', executer);
document.querySelector("#vider").addEventListener('click', raz);
document.addEventListener('keydown', function(event){
  if (event.keyCode == 32){
    executer();
  }
});
