let instructions = {
  '0' : {
    'message' : "Copie le mot mémoire d’adresse {adresse} dans le registre A",
    'operandes' : "adresse",
    'fonction' : function(adresse){
      if (adresse == "99"){
        let valeur = prompt("Valeur");
        set(99, valeur);
      }
      logger(this.message.replaceAll("{adresse}", adresse));
      set("A", get(parseInt(adresse)));
    }
  },
  '1' : {
    'message' : "Copie le mot mémoire d’adresse {adresse} dans le registre B",
    'operandes' : "adresse",
    'fonction' : function(adresse){
        if (adresse == "99"){
          let valeur = prompt("Valeur");
          set(99, valeur);
        }
        logger(this.message.replaceAll("{adresse}", adresse));
        set("B", get(parseInt(adresse)));
      }
  },
  '2' : {
    'message' :"Copie le contenu du registre R dans le mot mémoire d'adresse {adresse}",
    'operandes' : "adresse",
    'fonction' : function(adresse){
      logger(this.message.replaceAll("{adresse}", adresse));
      set(parseInt(adresse), get("R"));
      if (adresse == "99"){
        alert(get("R"));
      }
    }
  },
  '4' : {
    'message' : "Copie la valeur du registre source {rs} dans le registre destination {rd}",
    'operandes' : "rs rd",
    'fonction' : function(operande){
        let rs = registres[operande[0]];
        let rd = registres[operande[1]]
        logger(this.message.replaceAll("{rs}", rs).replaceAll('{rd}', rd));
        set(rd, get(rs));
        }
  },
  '5' : {
    'message' : "Branchement en {adresse} (PC reçoit la valeur {adresse})",
    'operandes' : "adresse",
    'fonction' : function(adresse){
      logger(this.message.replaceAll("{adresse}", adresse));
      set("PC", adresse);
      if (adresse == "99"){
        logger("Arrêt de la machine (PC vaut 99)");
      }
    }
  }
};

const _jeu = document.querySelector("#instructions tbody");
for (let operation in instructions) {
  let tr = document.createElement('tr');
  tr.innerHTML = "<td>" + operation + "</td><td><i>" + instructions[operation].operandes + "</i></td><td>" + instructions[operation].message + "</td>";
  _jeu.appendChild(tr);
}
