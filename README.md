---
title: Machine M999, le processeur débranché
author: Philippe BODDAERT
---

# Machine M999

## 1. Contexte

Un ordinateur est une __machine__ :

- dotée de composants __électroniques__, 
- exécute des __programmes__, stockés dans sa __mémoire__,
- les calculs sont réalisés par un __processeur__,
- interaction avec l'utilisateur par le biais de dispositifs d'__entrées/sorties__,

L'activité a pour objet l'utilisation et la programmation d'une machine __papier__ : ___M999___.

Cette machine est issue des travaux de Philippe Marquet et Martin Quinson [github.com/InfoSansOrdi/M999](http://github.com/InfoSansOrdi/M999), enseignants chercheurs en informatique.

## 2. Description

![Machine M999](./assets/memoire.png)

La machine M999 comporte :

- une __mémoire__ : 
	- 100 cases mémoires, numérotées de 00 à 99,
	- Chaque case peut contenir un __mot mémoire__ à 3 chiffres, correspondant à une instruction ou valeur selon le __jeu d'instructions__. 
- un __jeu d'instructions__ : Table qui associe à un __mot mémoire__ une instruction à réaliser.
- des __registres__ (__A, B, R__) : case mémoire contenant les valeurs d'opérandes, manipulées par les opérations,
- un __compteur d'instruction__ (__PC__) : contient l'adresse de l'instruction courante.

### 2.1. Jeu d'instructions

| op0   | op1 op2   | instruction à réaliser                                       |
| ----- | --------- | ------------------------------------------------------------ |
| 0     | _addr_    | copie le mot mémoire d’adresse _addr_ dans le registre A     |
| 1     | _addr_    | copie le mot mémoire d’adresse _addr_ dans le registre B     |
| 2     | _addr_    | copie le contenu du registre R dans le mot mémoire d'adresse _addr_ |
| 4     | _rs_ _rd_ | copie la valeur du registre source _rs_ dans le registre destination _rd_ |
| 5     | _addr_    | branche en _addr_ (PC reçoit la valeur _addr_)               |

Les registres (_rs_, _rd_) sont désignés par les valeurs suivantes :

valeur | registre
:------: | :--------:
0 | A
1 | B
2 | R

### 2.2. Exemple

Un exemple de machine M999 chargée avec un programme en mémoire :

![Exemple de machine M999](./assets/exemple.png)

### 2.3. Fonctionnement

- __Démarrage__ : la machine démarre avec la valeur 0 comme pointeur d'instruction (__PC__)
- La machine __charge__ l'instruction depuis la mémoire pointée par __PC__
- La machine __incrémente__ la valeur de __PC__
- La machine __décode__ l'instruction : à partir des 3 chiffres codant l'instruction, elle identifie l'opération à réaliser et les opérandes,
- La machine __exécute__ l'instruction.
- __Arrêt__ : La machine stoppe si __PC__ vaut __99__
- __Entrées / Sorties__ : 
  - Ècrire une valeur dans le mot mémoire __99__ l'affiche sur le terminal,
  - Les valeurs saisies sur le terminal sont lues dans le mot mémoire __99__. 

### 2.4. À Faire

1. En fonction du jeu d'instructions et de l'état de la mémoire donnés en exemple, exécutez la machine M999,
2. Que fait cette machine ?

## 3. Exercices

Écrire un programme en langage machine M999 consiste à décrire les valeurs de la mémoire qui permettent de réaliser le calcul.

Écrire les programmes en langage machine M999 suivants :

1. Ajouter 1 à un entier donné,
2. Calculer le minimum de 2 entiers donnés $`a`$ et $`b`$,
3. Calculer la parité d'un entier donné, (Pair, on affiche 1 dans le Terminal, Impair, on affiche 0)
4. Calculer la taille d'un entier donné,
4. Calculer le produit de 2 entiers
	- Par additions successives,
	- Par méthode paysanne russe.

## 4. Pour aller plus loin

Nous allons nous intéresser au coût en nombre d'opérations des programmes de la machine M999.

### 4.1. Rappel sur la notion de cycle

- Un cycle processeur se décompose en 3 étapes :
	- __Charger__ : charge l'instruction courante,
	- __Décoder__ : détermine l'opération et les opérandes,
	- __Exécuter__ : réalise l'opération.
- Horloge d'un processeur = Nombre de cycles par secondes, s'exprime en Hertz. (Exemple : un processeur doté d'une fréquence d'horloge de 3,2 GHz, exécute 3,2 milliards de cycles par seconde)
- Pour trouver le nombre d'instructions par seconde, on multiplie la fréquence d'horloge par l'IPC (nombre d'instructions par cycle).

### 4.2. Calcul du nombre de cycles

- Pour la M999, l'IPC est égal à 1 (une instruction par cycle),
- Déterminer l'horloge de la M999 est difficile car l'unité de commande et l'UAL correspondent à des actions humaines,
- On peut se poser la question __Combien de cycles sont nécessaires pour exécuter les programmes de la M999__ ?
- __Travail à réaliser__ : _Modifier les exercices pour que la machine renvoie le nombre de cycles exécutés pour effectuer les calculs_.

